// rodar yarn dev e yarn server ao mesmo tempo
// parar o yarn dev, rodar yarn build pro SSG funcionar e depois rodar yarn start
import Header from '../components/Header'
import '../styles/global.scss'
import styles from '../styles/app.module.scss'
import Player from '../components/Player'

function MyApp({ Component, pageProps }) {
  return (
    <div className={styles.wrapper}>
      <main>
        <Header />
        <Component {...pageProps} />
      </main>
      <Player />
    </div>
  )
}
export default MyApp
